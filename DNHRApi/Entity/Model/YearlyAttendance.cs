﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class YearlyAttendance
    {
        public int WorkingDays
        {
            get; set;
        }
        public string Emp_DeviceCode { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public int YearName { get; set; }
        public int Id { get; set; }
        public string Emp_Name { get; set; }
        public int LN1 { get; set; }
        public int LN2 { get; set; }
        public int LN3 { get; set; }
        public int LN4 { get; set; }
        public int LN5 { get; set; }
        public int LN6 { get; set; }
        public int LN7 { get; set; }
        public int LN8 { get; set; }
        public int LN9 { get; set; }
        public int LN10 { get; set; }
        public int LN11 { get; set; }
        public int LN12 { get; set; }
        public int A1 { get; set; }
        public int A2 { get; set; }
        public int A3 { get; set; }
        public int A4 { get; set; }
        public int A5 { get; set; }
        public int A6 { get; set; }
        public int A7 { get; set; }
        public int A8 { get; set; }
        public int A9 { get; set; }
        public int A10 { get; set; }
        public int A11 { get; set; }
        public int A12 { get; set; }
        public decimal L1 { get; set; }
        public decimal L2 { get; set; }
        public decimal L3 { get; set; }
        public decimal L4 { get; set; }
        public decimal L5 { get; set; }
        public decimal L6 { get; set; }
        public decimal L7 { get; set; }
        public decimal L8 { get; set; }
        public decimal L9 { get; set; }
        public decimal L10 { get; set; }
        public decimal L11 { get; set; }
        public decimal L12 { get; set; }
        public int EO1 { get; set; }
        public int EO2 { get; set; }
        public int EO3 { get; set; }
        public int EO4 { get; set; }
        public int EO5 { get; set; }
        public int EO6 { get; set; }
        public int EO7 { get; set; }
        public int EO8 { get; set; }
        public int EO9 { get; set; }
        public int EO10 { get; set; }
        public int EO11 { get; set; }
        public int EO12 { get; set; }
        public int st1 { get; set; }
        public int nd2 { get; set; }
        public int rd3 { get; set; }
        public int th4 { get; set; }
        public int th5 { get; set; }
        public int th6 { get; set; }
        public int th7 { get; set; }
        public int th8 { get; set; }
        public int th9 { get; set; }
        public int th10 { get; set; }
        public int th11 { get; set; }
        public int th12 { get; set; }
        public int Total { get; set; }
        public string Year { get; set; }
        public int Code { get; set; }
        public string Month { get; set; }
        public int DesignationId { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }

        public int LEA1 { get; set; }
        public int LEA2 { get; set; }
        public int LEA3 { get; set; }
        public int LEA4 { get; set; }
        public int LEA5 { get; set; }
        public int LEA6 { get; set; }
        public int LEA7 { get; set; }
        public int LEA8 { get; set; }
        public int LEA9 { get; set; }
        public int LEA10 { get; set; }
        public int LEA11 { get; set; }
        public int LEA12 { get; set; }
    }
}