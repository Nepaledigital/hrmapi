﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Entity.Model
{
    public class Employee
    {
        public Int64 SNo { get; set; }

        #region officeRecord
        public int Id { get; set; }
        [Display(Name = "Code")]
        public string Emp_Code { get; set; }
        [Display(Name = "Employee Name")]
        public string PhotoPath { get; set; }
        public int CountP { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_DeviceCode { get; set; }
        [DisplayName("Designation")]
        public int Designation_Id { get; set; }
        [DisplayName("Department")]
        public int Department_Id { get; set; }
        [DisplayName("Section")]
        public int Section_Id { get; set; }
        public int GradeGroup { get; set; }
        public bool IsManager { get; set; }


        #endregion
        #region BasicInformation

        [Display(Name = "Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public string FullPath { get; set; }

        public string DOB { get; set; }
        [DisplayName("Marital Status")]
        public string Marital_Status { get; set; }
        public string Gender { get; set; }
        [DisplayName("Blood Group")]
        public int Blood_Group { get; set; }
        [DisplayName("Mobile No")]
        public string Mobile_No { get; set; }
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }
        [DisplayName("PassPort No")]
        public string PassPort_No { get; set; }
        [DisplayName("CitizenShip No")]
        public string CitizenShip_No { get; set; }
        [DisplayName("Issued Date")]
        public string Issued_Date { get; set; }
        [DisplayName("Issued District")]
        public string Issued_District { get; set; }
        public int Religion { get; set; }
        public string photo { get; set; }
        [DisplayName("Permanet Address")]
        public string Permanet_Address { get; set; }
        [DisplayName("Permanet Address(Nepali)")]
        public string Permanet_Nepali { get; set; }
        [DisplayName("Temporary Address")]
        public string Temp_Address { get; set; }
        [DisplayName("Temporary Address(Nepali)")]
        public string Temp_Nepali { get; set; }

        public string FileLocation { get; set; }
        public string Marital_Status_Name { get; set; }
        public string GenderName { get; set; }
        public string Blood_Group_Name { get; set; }
        public string Religion_Name { get; set; }
        #endregion


        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string SectionName { get; set; }
        public IEnumerable<SelectListItem> DesignationList { get; set; }
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
        public IEnumerable<SelectListItem> SectionList { get; set; }
        public IEnumerable<SelectListItem> GradeGroupList { get; set; }

        public List<Allowance> Allowances { get; set; } = new List<Allowance>();
        public Allowance Allowance { get; set; }
        public DeductionRebateTax DeductionRebateTax { get; set; }
        public List<DeductionRebateTax> DeductionRebates { get; set; } = new List<DeductionRebateTax>();


        public IEnumerable<SelectListItem> AllowancesList { get; set; }
        public IEnumerable<SelectListItem> DeductionRebateList { get; set; }
        public IEnumerable<EmployeeSalaryInfo> EmployeeSalaryList { get; set; }
        public EmployeeSalaryInfo EmployeeSalaryInfo { get; set; }
        public IEnumerable<EmployeeAllowanceInfo> EmployeeAllowanceInfo { get; set; }


        public string Company_Code { get; set; }
        public char Event { get; set; }
        public bool Is_Active { get; set; }
        public DateTime Created_Date { get; set; }
        public int Created_By { get; set; }
    }

    public class EmployeeSalaryInfo
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Incentive { get; set; }
        public decimal OtherIncentive { get; set; }
        public bool WorkOnRemote { get; set; }
        public decimal RemoteRebate { get; set; }
        [Display(Name = "Contract Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime? FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ButtonName { get; set; }
    }

    public class EmployeeDeductionRebateInfo
    {
        public int Id { get; set; }
        public int SalaryId { get; set; }
        public int DeductionRebateId { get; set; }
        public string DeductionRebateName { get; set; }
        public int DeductionRebateOn { get; set; }
        public string DeductionRebateType { get; set; }
        public decimal DeductionRebate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class EmployeeAllowanceInfo
    {
        public int Id { get; set; }
        public int SalaryId { get; set; }
        public int AllowanceId { get; set; }
        public string AllowanceName { get; set; }
        public string AllowanceType { get; set; }
        public decimal Allowance { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class EmployeeAttendaceInfo
    {
        public string Date { get; set; }
        public string LoginTime { get; set; }
        public string LogOutTime { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public string OfficeIn { get; set; }
        public string OfficeOut { get; set; }
    }

    public class EmployeeLeaveInfo
    {
        public string LeaveName { get; set; }
        public string Date { get; set; }
        public string LeaveDay { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OfficeIn { get; set; }
        public string Status { get; set; }
    }

    public class EmployeePayrollInfo
    {
        public string FiscalYear { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public string Created_Date { get; set; }
        public int PresentDays { get; set; }
        public int LateInDays { get; set; }
        public int EarlyOutDays { get; set; }
        public int PaidLeaveDays { get; set; }
        public int UnpaidLeaveDays { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Incentive { get; set; }
        public decimal OtherIncentive { get; set; }
        public decimal ExtraTimeWork { get; set; }
        public decimal ExtraTimeWorkIncentive { get; set; }
        public decimal OverTime { get; set; }
        public decimal OverTimeIncentive { get; set; }
        public decimal RemoteRebate { get; set; }
        public decimal TotalAllowances { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal LeaveDaysDeductByLateIn { get; set; }
        public decimal SalaryDaysDeductByLateIn { get; set; }
        public decimal LeaveDaysDeductByEarlyOut { get; set; }
        public decimal SalaryDaysDeductByEarlyOut { get; set; }
        public decimal UnpaidLeaveDeduction { get; set; }
        public decimal AdvancedPayment { get; set; }
        public decimal GrossMonthlyIncome { get; set; }
        public decimal TotalTax { get; set; }
        public decimal MonthlyNetPay { get; set; }
    }

    public class EmployeeLeaveDetail
    {
        public int EmpId { get; set; }
        public string Emp_Name { get; set; }
        public string FY { get; set; }
        public int LeaveId { get; set; }
        public string LeaveName { get; set; }
        public decimal AssignDays { get; set; }
        public decimal TotalLeaveTaken { get; set; }
        public decimal RemainingLeave { get; set; }
        public string Pending { get; set; }
    }

    public class LeaveDetail
    {
        public string EmployeeName { get; set; }
        public string LeaveName { get; set; }
        public decimal AssignedLeaveDays { get; set; }
        public int LeaveId { get; set; }
        public int EmpId { get; set; }

        public IEnumerable<LeaveTakenDetail> LeaveTakenDetail { get; set; }
    }
    public class LeaveTakenDetail
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromDateNepali { get; set; }
        public string ToDateNepali { get; set; }
        public decimal TakenLeaveDays { get; set; }
        public string Status { get; set; }
    }

    public class FiscalYear
    {
        public int Id { get; set; }
        public string YearNepali { get; set; }
        public string Year { get; set; }
        public bool CurrentFiscalYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDateNepali { get; set; }
        public DateTime EndDateNepali { get; set; }
        public bool IsActive { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}