﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Entity.Model

{
    public class Company
	{
		public Int64 SNo { get; set; }
        public int CountP { get; set; }
        public string FullPath { get; set; }

        public int? Id { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [Display(Name = "Company code")]
        [Remote("GetCode", "Company", HttpMethod = "POST", ErrorMessage = "This Code already exists.")]
        public string CompanyCode { get; set; }
        [Display(Name = "Company Address")]
        [Required]
        [StringLength(50)]
        public string CompanyAddress { get; set; }
        [Display(Name = "Phone No")]
        [Required]
        public string PhoneNo { get; set; }
        [EmailAddress]
        [Display(Name = "Email Address")]
        [Required]
        public string Email { get; set; }
        [Display(Name = "Web Address")]
        [Required]
        public string Web { get; set; }
        public string Logo { get; set; }
        public char Event { get; set; }
        public bool IsDeleted { get; set; }
        public string TaxationNumber { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
    public class CompanyViewModel
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [Display(Name = "Company code")]
        public string CompanyCode { get; set; }
        [Display(Name = "Company Address")]
        [Required]
        [StringLength(50)]
        public string CompanyAddress { get; set; }
        [Display(Name = "Phone No")]
        [Required]
        public string PhoneNo { get; set; }
        [EmailAddress]
        [Display(Name = "Email Address")]
        [Required]
        public string Email { get; set; }
        [Display(Name = "Web Address")]
        [Required]
        public string Web { get; set; }
        public string Logo { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }

}