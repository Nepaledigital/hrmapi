﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class RemoteAttendance
    {
        public int Id { get; set; }
        public DateTime DateEng { get; set; }
        public string DateNep { get; set; }
        public int EmployeeId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int Distance { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }

        public int BranchId { get; set; }
        public int Department_Id { get; set; }
        public int Designation_Id { get; set; }
        public int Section_Id { get; set; }

        public string[] EmployeeIds { get; set; }
    }
}