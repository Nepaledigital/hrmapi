﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class AttendanceReport
    {
        public int LateIn { get; set; }
        public int EarlyOut { get; set; }
        public int TotalAbsent { get; set; }
        public int TotalPresent { get; set; }
        public string Date { get; set; }
    }
    public class OnLeaveEmployee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
    }
    public class LeaveReport
    {
        public int Approved { get; set; }
        public int Pending { get; set; }
        public int Rejected { get; set; }
    }
    public class LateInList
    {
        public int EmpId { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public string Emp_Name { get; set; }
        public string DesignationName { get; set; }
        public int Code { get; set; }
        public DateTime date { get; set; }
        public string DeviceIP { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string AllowedLateTime { get; set; }
        public string AllowedEarlyTime { get; set; }
        public string ShiftStart { get; set; }
        public string IpAddress { get; set; }
        public string ShiftEnd { get; set; }
        public DateTime LogInTime { get; set; }
        public DateTime LoginDate { get; set; }
        public int LateIn { get; set; }
        public int EarlyOut { get; set; }
    }
    public class EarlyoutList
    {
        public int EmpId { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public string Emp_Name { get; set; }
        public string DesignationName { get; set; }
        public int Code { get; set; }
        public DateTime date { get; set; }
        public string DeviceIP { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string AllowedLateTime { get; set; }
        public string AllowedEarlyTime { get; set; }
        public string ShiftStart { get; set; }
        public string IpAddress { get; set; }
        public string ShiftEnd { get; set; }
        public DateTime LogInTime { get; set; }
        public DateTime LoginDate { get; set; }
        public int LateIn { get; set; }
        public int EarlyOut { get; set; }
    }

    public class AbsentList
    {
        public string Date { get; set; }
        public string Emp_Name { get; set; }
        public int Id { get; set; }
        public int Emp_DeviceCode { get; set; }
        public string WeeklyOff { get; set; }
        public string Status { get; set; }
    }

}