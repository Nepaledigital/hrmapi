﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class Attendance
    {
        public string AllowedLateInTime { get; set; }
        public string AllowedEarlyOutTime { get; set; }
        public string AllowedLateTime { get; set; }
        public string AllowedEarlyTime { get; set; }
        public string OverTime { get; set; }
        public string ExtraTime { get; set; }

        public string EarlyLateIn { get; set; }
        public string EarlyLateOut { get; set; }
        public int EarlyOutAbsent { get; set; }
        public int LateInAbsent { get; set; }
        public string Holiday { get; set; }
        public string Leave { get; set; }
        public string Weekend { get; set; }

        public int Id { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public string DeviceIP { get; set; }
        public DateTime LogInTime { get; set; }
        public string LogInTimeNep { get; set; }
        public DateTime LoginOutTime { get; set; }
        public DateTime LogTime { get; set; }
        public string Emp_Name { get; set; }
        public string WorkingHour { get; set; }
        public string DepartmentName { get; set; }

        public string DesignationName { get; set; }
        public string ActualworkedHour { get; set; }
        public string OutTime { get; set; }
        public string InTime { get; set; }

        public int DesignationId { get; set; }
        public int DepartmentId { get; set; }
        public string Date { get; set; }
        public string DateNepali { get; set; }

        public string Year { get; set; }
        public string Month { get; set; }
        public int WeekNumber { get; set; }
        public string Result { get; set; }
        public string Days { get; set; }
        public int YearValue { get; set; }
        public int TotalPresent { get; set; }

        public int Code { get; set; }

        public int Jan { get; set; }
        public int Feb { get; set; }
        public int Mar { get; set; }
        public int Apr { get; set; }
        public int May { get; set; }
        public int Jun { get; set; }
        public int Jul { get; set; }
        public int Aug { get; set; }
        public int Sep { get; set; }
        public int Oct { get; set; }
        public int Nov { get; set; }
        public int Dec { get; set; }


        public string WeeklyOff { get; set; }
        public string ShiftHours { get; set; }
        public string Name { get; set; }
        public string ShiftType { get; set; }
        public TimeSpan AShiftStart { get; set; }
        public TimeSpan AShiftEnd { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public string AllowLateIn { get; set; }
        public string AllowEarlyOut { get; set; }
        public int LateIn { get; set; }
        public int EarlyOut { get; set; }
        public string DiffernceHour { get; set; }
        public string Remarks { get; set; }
        public string Emp_Code { get; set; }
        public Int64 Sno { get; set; }

        public DateTime AttendanceDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string Remark { get; set; }
        public string Type { get; set; }
        public string Mode { get; set; }
        public string Status { get; set; }
        public DateTime StartDateNepali { get; set; }
        public DateTime EndDateNepali { get; set; }
    }
}