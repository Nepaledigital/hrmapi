﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class UserDetail
    {
        public int Id { get; set; }
        public string FullPath { get; set; }
        public int Count { get; set; }
        public int EmpId { get; set; }
        public string Emp_Code { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_DeviceCode { get; set; }
        public DateTime DOB { get; set; }
        [RegularExpression(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }
        public string CitizenShip_No { get; set; }
        public string photo { get; set; }
        public string Temp_Address { get; set; }
        public string Permanet_Address { get; set; }
        public string GenderName { get; set; }
        public string Marital_Status_Name { get; set; }
        public string Blood_Group_Name { get; set; }
        public string Mobile_No { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string PassWord { get; set; }
        public string ImgPath { get; set; }
        [RegularExpression(@"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}", ErrorMessage = "Invalid Email")]
        public string UserName { get; set; }
        public string Company_Code { get; set; }
        public string RoleName { get; set; }

    }
}