﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity
{
    public class Holiday
    {
        public Int64 SNo { get; set; }
        public string[] DepartmentId { get; set; }
        public string Departments { get; set; }

        public int Id { get; set; }
        public string HolidayName { get; set; }
        public string HolidayType { get; set; }
        public string ApplicableReligion { get; set; }
        public string ApplicableGender { get; set; }
        public string Description { get; set; }

        public DateTime FromDate { get; set; }
        public string FromDateNepali { get; set; }

        public DateTime ToDate { get; set; }
        public string ToDateNepali { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Remarks { get; set; }
        public char Event { get; set; }
        public bool IsDeleted { get; set; }
        public bool DataExist { get; set; }

    }
    public class LeaveDetails
    {
        public DateTime LeaveToDate { get; set; }
        public DateTime LeaveFromDate { get; set; }
        public String LeaveName { get; set; }
        public int EmployeeId { get; set; }
        public String Code { get; set; }
        public string LeaveDay { get; set; }
    }
}