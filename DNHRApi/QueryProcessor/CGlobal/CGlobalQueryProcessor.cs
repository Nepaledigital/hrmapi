﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
namespace QueryProcessor
{
	public class CGlobalQueryProcessor
	{


		public static Dictionary<string, string> BindDropdownItems(string Query, string ComCode)
		{
			var dbfactory = DbFactoryProvider.GetFactory();
			Dictionary<string, string> dict = new Dictionary<string, string>();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = Query;
                cmd.Connection = (SqlConnection)dbfactory.GetConnection(ComCode);
                cmd.Connection.Open();
                using (var dr = cmd.ExecuteReader())
                {

                    while (dr.Read())
                    {
                        dict[dr["DataValueField"].ToString()] = dr["DataTextField"].ToString();

                    }
                }
                cmd.Connection.Close();
            }
			return dict;
		}


	}
}