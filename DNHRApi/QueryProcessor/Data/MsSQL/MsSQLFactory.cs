using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using QueryProcessor;
using QueryProcessor.DropDown;

namespace QueryProcessor
{
    /// <summary>
    /// 
    /// </summary>
    public class MsSQLFactory : IDatabaseFactory
    {
        public IDbConnection Db { get; set; }
        public Dialect Dialect => Dialect.SQLServer;

        public QueryBuilder QueryBuilder { get; }

        private readonly string _connectionString;
        public MsSQLFactory()
        { }
        public IDbConnection GetConnection(string CompanyCode)
        {
            // var ss= new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
            //var tt = new SqlConnection(General.BuildDyanmicString("003").ToString());
            //ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            //Db = new SqlConnection(General.BuildDyanmicString(Info._companyCode).ToString());
            Db = new SqlConnection(General.BuildDyanmicString(CompanyCode).ToString());

            //Db =new SqlConnection(General.BuildDyanmicString("003").ToString());

            return Db;
        }
        //in appsetting file
        //      "DBInfo": {
        //  "Name": "coresample",
        //  "ConnectionString": "User ID=postgres;Password=xxxxxx;Host=localhost;Port=5432;Database=coresample;Pooling=true;"
        //}
        public MsSQLFactory(IConfiguration configuration, IServiceProvider serviceProvider)
        {
            //IConfiguration configuration
            // connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
            _connectionString = configuration.GetConnectionString("DefaultConnection");// ConfigurationManager.ConnectionStrings[connectionStringName].ToString();
            Db = new SqlConnection(_connectionString);
            QueryBuilder = new MsSqlQueryBuilder(new MsSQLTemplate());
            //var hostingenv = serviceProvider.GetService<IHostingEnvironment>();
            //DbLogger = new DbLogger(hostingenv,new DefaultDbLoggerSetting());
        }

        public void Dispose()
        {
            if (Db.State == ConnectionState.Open)
                Db.Close();
            Db.Close();
            //db.Dispose();
        }
    }


}