using System;

namespace QueryProcessor
{
    public interface ITableNameResolver
    {
        string ResolveTableName(Type type);
    }
}