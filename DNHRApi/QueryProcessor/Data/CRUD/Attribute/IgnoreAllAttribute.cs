﻿using System;

namespace JoinAttribute
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreAllAttribute : System.Attribute
    {
    }
}