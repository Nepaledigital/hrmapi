using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace QueryProcessor
{
    public class CrudService<T>
    {
        public CrudService(IDatabaseFactory dbFactory)
        {
            DbFactory = dbFactory;
        }
        public CrudService()
        {
            DbFactory = DbFactoryProvider.GetFactory();
        }

        private IDatabaseFactory DbFactory { get; }
        //public Task<int> SaveAsync(T t)
        //{
        //    using (var db = DbFactory.Open())
        //    {
        //        var x = await db.GetListAsync<T>();

        //    }
        //}

        public virtual T Query(string sql, string comCode, object param = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                db.Open();
                var result = db.Query<T>(sql, param);
                db.Close();
                return result.FirstOrDefault();
            }
        }
        public virtual  IEnumerable<T> QueryAsync(string sql, string comCode, object param = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                var result =  db.Query<T>(sql, param);
                db.Close();
                return result;

            }
        }
        public virtual IEnumerable<T> QueryList(string sql, string comCode, object param = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                db.Open();
                return db.Query<T>(sql, param);
            }
        }
        public virtual  IEnumerable<T> QueryListAsync(string sql, string comCode, object param = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return  db.Query<T>(sql, param);

            }
        }
        public virtual T Get(object id, string comCode)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                db.Open();
                return db.Get<T>(id);

            }
        }
        public virtual  IEnumerable<T> GetAsync(object id, string comCode)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return  db.GetAsync<T>(id);

            }
        }
        public virtual T Get(string condition, string comCode, object parameters = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                db.Open();
                return db.Get<T>(condition, parameters);

            }
        }
        public virtual  IEnumerable<T> GetAsync(string condition, string comCode, object parameters = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return  db.GetAsync<T>(condition, parameters);

            }
        }

        public virtual IEnumerable<T> GetList(object whereConditions, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.GetList<T>(whereConditions);

                }
            }
        }
        public virtual  IEnumerable<T> GetListAsync(object whereConditions, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.GetListAsync<T>(whereConditions);

                }
            }
        }

        public virtual IEnumerable<T> GetList(string conditions,  string comCode,
           object parameters)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.GetList<T>(conditions, parameters);

                }
            }
        }
        public virtual  IEnumerable<T> GetListAsync(string conditions,  string comCode,
            object parameters)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.GetListAsync<T>(conditions, parameters);
                }
            }
        }
        public virtual  IEnumerable<T> GetJoinedList(string conditions, string comCode,
           object parameters = null)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.GetJoinedList<T>(conditions, parameters);

                }
            }
        }

        public virtual IEnumerable<T> GetList(string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.GetList<T>();

                }
            }
        }
        public virtual  IEnumerable<T> GetListAsync(string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.GetListAsync<T>();

                }
            }
        }

        public virtual IEnumerable<T> GetListPaged(int pageNumber, int rowsPerPage, string comCode,
           int pageSize, string conditions, string orderby, object parameters = null)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.GetListPaged<T>(pageNumber, rowsPerPage, pageSize, conditions, orderby, parameters);

                }
            }
        }
        public virtual  IEnumerable<T> GetListPagedAsync(int pageNumber, int rowsPerPage, string comCode,
            int pageSize, string conditions, string orderby, object parameters = null)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.GetListPagedAsync<T>(pageNumber, rowsPerPage, pageSize, conditions, orderby, parameters);

                }
            }
        }

        public virtual int? Insert(object entityToInsert,string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.Insert<int?>(entityToInsert);

                }
            }
        }
        public virtual  int? InsertAsync(object entityToInsert, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.InsertAsync<int?>(entityToInsert);

                }
            }
        }

        public virtual TKey Insert<TKey>(object entityToInsert,string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.Insert<TKey>(entityToInsert);

                }
            }
        }
        public virtual  TKey InsertAsync<TKey>(object entityToInsert, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.InsertAsync<TKey>(entityToInsert);

                }
            }
        }
        public virtual  TKey InsertAsync<TKey>(IDbConnection db, object entityToInsert, IDbTransaction transaction, int? commandTimeout)
        {
            {

                return  db.InsertAsync<TKey>(entityToInsert, transaction, commandTimeout);

            }
        }

        public virtual int Update(object entityToUpdate, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.Update(entityToUpdate);

                }
            }
        }
        public virtual int Update(IDbConnection db, object entityToUpdate, IDbTransaction transaction, int? timeout)
        {
            {

                return db.Update(entityToUpdate, transaction, timeout);


            }
        }
        public virtual  int UpdateAsync(IDbConnection db, object entityToUpdate, IDbTransaction transaction, int? commandTimeout)
        {
            {

                return  db.UpdateAsync(entityToUpdate, transaction, commandTimeout);


            }
        }
        public  int UpdateAsync(string comCode, object entityToUpdate, string condition, object paramters = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return db.UpdateAsync(entityToUpdate, condition, paramters);

            }
        }
        public virtual  Task<bool> UpdateAsync(object entityToUpdate, string comCode)
        {

            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                 db.UpdateAsync(entityToUpdate);
                return  Task.FromResult(true);
            }

        }
        public virtual int Delete(T entityToDelete, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.Delete(entityToDelete);

                }
            }
        }
        public virtual  int DeleteAsync(T entityToDelete,string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.DeleteAsync(entityToDelete);

                }
            }
        }
        public virtual int Delete(object id, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.Delete<T>(id);

                }
            }
        }
        public virtual int Delete(IDbConnection db, object id, IDbTransaction transaction, int? timeout)
        {

            return db.Delete(id, transaction, timeout);
        }
        public virtual  int DeleteAsync(object id, string comCode)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return  db.DeleteAsync<T>(id);

            }

        }
        public virtual  int DeleteAsync(IDbConnection db, object id, IDbTransaction transaction, int? timeout)
        {
            return  db.DeleteAsync(id, transaction, timeout);
        }
        public virtual int Delete(string comCode, int[] ids)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    db.Open();
                    return db.Delete<T>(ids);

                }
            }
        }
        public virtual  int DeleteAsync(int[] ids, string comCode)
        {

            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return  db.DeleteAsync<T>(ids);

            }

        }
        public virtual  int DeleteAsync(string comCode, string condition, object parameters = null)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return db.DeleteAsync<T>(condition, parameters);
            }
        }
        public virtual  int DeleteAsync(IDbConnection db, IDbTransaction transaction, string condition, object parameters, int? timeout)
        {
            return db.DeleteAsync<T>(condition, parameters, transaction, timeout);
        }
        // int DeleteList(object whereConditions);
        public virtual  int DeleteListAsync(object whereConditions, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.DeleteListAsync<T>(whereConditions);

                }
            }
        }
        public virtual  int DeleteListAsync(string conditions, string comCode,
            object parameters = null)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.DeleteListAsync<T>(conditions, parameters);

                }
            }
        }

        public virtual  int RecordCountAsync(string comCode, string conditions = "", object parameters = null)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.RecordCountAsync<T>(conditions, parameters);

                }
            }
        }
        public virtual  int RecordCountAsync(object whereConditions, string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                     db.OpenAsync();
                    return  db.RecordCountAsync<T>(whereConditions);

                }
            }
        }

        public virtual async Task<object> GetDependents(string comCode)
        {
            {
                using (var db = (DbConnection)DbFactory.GetConnection(comCode))
                {
                    await db.OpenAsync();
                    return await db.GetDependents<T>();
                }
            }
        }



        public virtual  int UpdateAsDeleted(string comCode, object id)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
               return  db.UpdateAsDeleted<T>(id);
            }
        }

        public virtual  int UpdateStatusAsync(string comCode, object id, object status)
        {
            using (var db = (DbConnection)DbFactory.GetConnection(comCode))
            {
                 db.OpenAsync();
                return  db.UpdateStatus<T>(id, status);
            }
        }
    }
}