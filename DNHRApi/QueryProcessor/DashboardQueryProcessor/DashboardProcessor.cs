﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace QueryProcessor.DashboardQueryProcessor
{
    public class DashboardProcessor : IDashboardProcessor
    {

        AttendanceReport IDashboardProcessor.GetAllData(string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var param = new DynamicParameters();
                        param.Add("@BranchId", 0);
                        var dailyAttendanceReport = db.Query<AttendanceReport>(sql: "[dbo].[DailyAttendanceReport]",param:param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        return dailyAttendanceReport;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    return null;
                }

            }
        }
        public IEnumerable<OnLeaveEmployee> GetEmployeeOnLeave(string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var data = db.Query<OnLeaveEmployee>(sql: "[dbo].[GetEmployeeOnLeave]", commandType: CommandType.StoredProcedure);
                        foreach (var item in data)
                        {
                            if(!string.IsNullOrEmpty(item.Photo)){
                                var ImgPath = item.Photo.Replace('\\', '/');
                                //item.Photo = "www.onlinehajiri.com" + ImgPath;

                                var request = HttpContext.Current.Request;
                                var appRootFolder = request.ApplicationPath;
                                if (!appRootFolder.EndsWith("/"))
                                {
                                    appRootFolder += "/";
                                }
                                var url = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appRootFolder);
                                item.Photo = url + ImgPath;
                            }
                        }
                        return data;

                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

            }
        }
        public LeaveReport GetLeaveReport(string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var data = db.Query<LeaveReport>(sql: "[dbo].[EmployeeLeaveReport]", commandType: CommandType.StoredProcedure).SingleOrDefault();
                        return data;

                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

            }
        }
        public LeaveReport GetLeaveReportWeekly(string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var data = db.Query<LeaveReport>(sql: "[dbo].[EmployeeLeaveReportweekly]", commandType: CommandType.StoredProcedure).SingleOrDefault();
                        return data;

                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

            }
        }
        public LeaveReport GetLeaveReportMonthly(string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var data = db.Query<LeaveReport>(sql: "[dbo].[EmployeeLeaveReportMonthly]", commandType: CommandType.StoredProcedure).SingleOrDefault();
                        return data;

                    }
                    catch (Exception)
                    {
                        return null;
                    }

                }
                else
                {
                    return null;
                }
            }
        }
    }
}