﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QueryProcessor
{
	public class CGlobalModels
	{
		#region Bind ddl

		public static List<SelectListItem> BindShift(string Id, string Code1, string Code2, string Station_Id, string ComCode)
		{

			string query = @"SELECT [Id] DataValueField ,[Name] DataTextField FROM [dbo].[Shift] where IsDeleted = 0";
			// get the regions from database
			Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query, ComCode);
			// generate the dropdown items
			List<SelectListItem> ddlItems = new List<SelectListItem>();
			ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
			foreach (var key in regions.Keys)
			{
				ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
			}

			return ddlItems;
		}

        public static List<SelectListItem> BindAllowances(string Id, string Code1, string Code2, string Station_Id, string ComCode)
        {
            string query = @"SELECT [Id] DataValueField ,[Name] DataTextField from [dbo].[Allowance] where Is_Active = 1";
            // get the regions from database
            Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query, ComCode);
            // generate the dropdown items
            List<SelectListItem> ddlItems = new List<SelectListItem>();
            ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Allowances", Value = "0" });
            foreach (var key in regions.Keys)
            {
                ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
            }

            return ddlItems;
        }

        public static List<SelectListItem> BindDeductionRebate(string Id, string Code1, string Code2, string Station_Id, string ComCode)
        {
            string query = @"SELECT [Id] DataValueField ,[Name] DataTextField from [dbo].[DeductionRebateTax] where Is_Active = 1"; 
            // get the regions from database
            Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query, ComCode);
            // generate the dropdown items
            List<SelectListItem> ddlItems = new List<SelectListItem>();
            ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Deduction/Rebate", Value = "0" });
            foreach (var key in regions.Keys)
            {
                ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
            }

            return ddlItems;
        }
        #endregion
    }
}