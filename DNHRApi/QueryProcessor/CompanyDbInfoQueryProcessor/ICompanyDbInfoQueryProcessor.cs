﻿using Entity.Model;
using System.Collections.Generic;


namespace QueryProcessor
{
	public interface ICompanyDbInfoQueryProcessor
	{
		int Create(CompanyDatabaseInfo obj);
		IEnumerable<CompanyDatabaseInfo> GetAllData();
		CompanyDatabaseInfo GetById(int id);
		
	}
}
