﻿using Entity.Model;
using System.Collections.Generic;

namespace QueryProcessor
{
public	interface ILoginProcessor
	{
         IEnumerable<UserDetail> Authenticate(string UserName, string Password);
        UserDetail UserDetail(string UserName, string Password, string ComCode);
        //void GetSetTemplateValue();
    }
}
