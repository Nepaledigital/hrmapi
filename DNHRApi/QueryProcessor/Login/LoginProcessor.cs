﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Entity.Model;
using QueryProcessor.DropDown;

namespace QueryProcessor
{
    public class LoginProcessor : ILoginProcessor
    {
        //MenuQueryProcessor mn = new MenuQueryProcessor();
        public IEnumerable<UserDetail> Authenticate(string UserName, string Password)
        {
            try
            {
               
                HttpContext context = System.Web.HttpContext.Current;
                var dbfactory = DbFactoryProvider.GetFactory();
                var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
                db.Open();
                string pass = General.Encrypt(Password);
                var param = new DynamicParameters();
                param.Add("@UserName", UserName);
                param.Add("@Password", General.Encrypt(Password));
               
             
                var data = db.Query<UserDetail>(sql: "[dbo].[Usp_MobAuthenticate]", param: param, commandType: CommandType.StoredProcedure);
                db.Close();
                if (data.Count() > 0)
                {
                    string code = data.Select(x => x.Company_Code).First();
                    string[] values = code.Split('-');
                    HttpContext.Current.Session["UserId"] = Convert.ToInt32(values[1]);
                }
                return data;



            }
            catch (Exception)
            {
                return null;

            }
        }
        public UserDetail UserDetail(string UserName, string Password, string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    var obj = new UserDetail();
                    db.Open();
                    var param = new DynamicParameters();
                    var usrId = HttpContext.Current.Session["UserId"];
                    param.Add("@Id", HttpContext.Current.Session["UserId"]);
                    var UserDetails = db.Query<UserDetail>(sql: "[dbo].[GetOverAllEMployeeDetails]", param: param, commandType: CommandType.StoredProcedure);

                    var imgName = "";
                    var imgFullPath = "";
                    if (!string.IsNullOrEmpty(UserDetails.FirstOrDefault().photo))
                    {
                        var ImgPath = UserDetails.FirstOrDefault().photo.Replace('\\', '/');
                        //imgName = "www.onlinehajiri.com/" + ImgPath;
                        var request = HttpContext.Current.Request;
                        var appRootFolder = request.ApplicationPath;
                        if (!appRootFolder.EndsWith("/"))
                        {
                            appRootFolder += "/";
                        }
                        var url = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appRootFolder);
                        imgName = url + ImgPath;
                    }

                    if (UserDetails.Count() > 0)
                    {
                        obj.Company_Code = ComCode.Split('-').First();
                        obj.UserName = UserDetails.Select(x => x.UserName).First();

                        obj.EmpId = Convert.ToInt32(UserDetails.Select(x => x.EmpId).First());
                        obj.Emp_Code = UserDetails.Select(x => x.Emp_Code).First();
                        obj.Emp_Name = UserDetails.Select(x => x.Emp_Name).First();
                        obj.Emp_DeviceCode = UserDetails.Select(x => x.Emp_DeviceCode).First();
                        obj.DOB = UserDetails.Select(x => x.DOB).First();
                        obj.Email = UserDetails.Select(x => x.Email).First();
                        obj.CitizenShip_No = UserDetails.Select(x => x.CitizenShip_No).First();

                        obj.DOB = UserDetails.Select(x => x.DOB).First();
                        obj.Temp_Address = UserDetails.Select(x => x.Temp_Address).First();
                        obj.Permanet_Address = UserDetails.Select(x => x.Permanet_Address).First();
                        obj.Mobile_No = UserDetails.Select(x => x.Mobile_No).First();
                        obj.Email = UserDetails.Select(x => x.Email).First();
                        obj.GenderName = UserDetails.Select(x => x.GenderName).First();
                        obj.CitizenShip_No = UserDetails.Select(x => x.CitizenShip_No).First();
                        obj.Marital_Status_Name = UserDetails.Select(x => x.Marital_Status_Name).First();
                        obj.Blood_Group_Name = UserDetails.Select(x => x.Blood_Group_Name).First();
                        obj.Mobile_No = UserDetails.Select(x => x.Mobile_No).First();

                        obj.UserId = UserDetails.Select(x => x.UserId).First();
                        obj.RoleId = UserDetails.Select(x => x.RoleId).First();
                        obj.Temp_Address = UserDetails.Select(x => x.Temp_Address).First();
                        obj.PassWord = UserDetails.Select(x => x.PassWord).First();
                        obj.photo = imgName;
                        obj.FullPath = imgFullPath;

                    }
                    return obj;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public string GetCompCodeByEmpId(int Id)
        {

            try
            {
                Employee obj = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
                var param = new DynamicParameters();
                var param1 = new DynamicParameters();
                db.Open();
                param.Add("@Id", Id);
                var Employeelist = db.Query<Employee>(sql: "[dbo].[GetCompCodeByEmpId]", param: param, commandType: CommandType.StoredProcedure);
                string code = string.Empty;
                if (Employeelist.Count() > 0)
                {
                    
                    code = Employeelist.Select(x => x.Company_Code).First();
                    HttpContext.Current.Session["Code"] = code;
                    string[] values = code.Split('-');
                    Info._companyCode = values[0];
                    // Info._empid= Convert.ToInt32(values[1]);
                    HttpContext.Current.Session["UserId"] = Convert.ToInt32(values[1]);
                    HttpContext.Current.Session["ImgPath"] = "\\" + Employeelist.Select(x => x.PhotoPath).First();
                    var imgPath = HttpContext.Current.Session["ImgPath"];
                }

                db.Close();
                return code;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}