﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Model;

namespace QueryProcessor.QueryProcessor
{
    public interface IEmployee
    {
        int Create(Employee employee, string ComCode);
        IEnumerable<Employee> GetAllData(string ComCode);
        Employee GetById(int id, string ComCode);
        bool Delete(int id, string ComCode);
    }
}
