﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace QueryProcessor.QueryProcessor.EmployeeQueryProcessor
{
    public class EmployeeProcessor : IEmployee
    {
        public int Create(Employee employee, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", employee.Event);
                    param.Add("@Id", employee.Id);
                    param.Add("@Name", employee.Emp_Name);
                    db.Execute("[dbo].[Usp_IUD_Allowance]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Allowance] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public IEnumerable<Employee> GetAllData(string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    db.Open();
                    var allowancelist = db.Query<Employee>(sql: "[dbo].[Usp_GetAllowancelist]", commandType: CommandType.StoredProcedure);
                    return allowancelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Employee GetById(int id, string ComCode)
        {
            try
            {
                Employee obj = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var allowancelist = db.Query<Employee>(sql: "[dbo].[Usp_GetAllowanceById]", param: param, commandType: CommandType.StoredProcedure);
                    if (allowancelist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(allowancelist.Select(x => x.Id).First());
                        obj.Emp_Name = allowancelist.Select(x => x.Emp_Name).First();
                        obj.Permanet_Address = allowancelist.Select(x => x.Permanet_Address).First();
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}