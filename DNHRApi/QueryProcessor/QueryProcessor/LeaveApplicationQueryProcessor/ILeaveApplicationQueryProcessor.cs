﻿
using Entity.Model;
using System.Collections.Generic;


namespace QueryProcessor.QueryProcessor
{
	public	interface ILeaveApplicationQueryProcessor
	{
		int Create(LeaveApplication obj, string ComCode);
		IEnumerable<LeaveApplication> GetAllData(string ComCode);
		IEnumerable<LeaveApplication> GetAllDataByStatus(string ComCode);
		LeaveApplication GetById(int id, string ComCode);
		bool Delete(int id,string ComCode);
        bool IsLeaveTaken(string date, int id, string leaveId, string ComCode);

        bool LeaveApproval(int id,char approval,string approvedBy, string ComCode);
		bool LeaveRequestStatusIsPending(int empId, int leaveId, string laId,string ComCode);
		LeaveApplication GetByEmpAndLeave(string empId, string leaveId, string ComCode);
        bool IsLeaveUnApproved(int id, int Lid, int laID, string ComCode);
        void IsLastLeave(int lAid, string approvedStatus, string ComCode);
        decimal IsMoreLeaveAvailable(int empid, int lId, int laId, string ComCode);
        IEnumerable<EmployeeLeaveReport> GetLeaveReport(int fiscalyearId, int EmpId, int DepId, char LeaveStatus, string ComCode);
        IEnumerable<EmployeeLeaveReport> GetDetailLeaveReport(int EmpId, int LeaveAssignId, string ComCode);
    }
}
