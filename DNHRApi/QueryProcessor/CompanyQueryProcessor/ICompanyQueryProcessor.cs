﻿using Entity.Model;
using System.Collections.Generic;

namespace QueryProcessor
{
  public  interface ICompanyQueryProcessor
    {
        int Create(Company company);
        IEnumerable<CompanyViewModel> GetAllData();
        Company GetById(int id);
        bool Delete(int id);
        int GetCode(string code,int id);
        CompanyDatabaseInfo GetByCode(string code);
        CompanyDatabaseInfo GetCompanyByCode(string code);
        string GetRoleNameByEmpId(int EmpId, string ComCode);
    }
}
