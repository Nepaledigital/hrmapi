﻿
using Entity.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QueryProcessor
{
    public class EmployeeQueryProcessor : IEmployeeQueryProcessor
    {
        public IEnumerable<Employee> GetAllData(string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        db.Open();
                        var Employeelist = db.Query<Employee>(sql: "[dbo].[Usp_GetEmployeeList]", commandType: CommandType.StoredProcedure);
                        return Employeelist;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Employee GetById(int id, string ComCode)
        {
            try
            {
                Employee obj = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@Id", id);
                        var Employeelist = db.Query<Employee>(sql: "[dbo].[Usp_GetEmployeeById]", param: param, commandType: CommandType.StoredProcedure);
                        var imgName = "";
                        var imgFullPath = "";
                        if (!string.IsNullOrEmpty(Employeelist.FirstOrDefault().photo))
                        {
                            var ImgPath = Employeelist.FirstOrDefault().photo.Replace('\\', '/');
                            //imgName = "www.onlinehajiri.com/" + ImgPath;
                            var request = HttpContext.Current.Request;
                            var appRootFolder = request.ApplicationPath;
                            if (!appRootFolder.EndsWith("/"))
                            {
                                appRootFolder += "/";
                            }
                            var url =  string.Format("{0}://{1}{2}",request.Url.Scheme,request.Url.Authority,appRootFolder);
                            imgName = url + ImgPath;
                        }

                        if (Employeelist.Count() > 0)
                        {
                            obj.Id = Convert.ToInt32(Employeelist.Select(x => x.Id).First());
                            obj.Emp_Code = Employeelist.Select(x => x.Emp_Code).First();
                            obj.Emp_Name = Employeelist.Select(x => x.Emp_Name).First();
                            obj.Emp_DeviceCode = Employeelist.Select(x => x.Emp_DeviceCode).First();
                            obj.Designation_Id = Employeelist.Select(x => x.Designation_Id).First();
                            obj.Department_Id = Employeelist.Select(x => x.Department_Id).First();
                            obj.Section_Id = Employeelist.Select(x => x.Section_Id).First();
                            obj.GradeGroup = Employeelist.Select(x => x.GradeGroup).First();
                            obj.IsManager = Employeelist.Select(x => x.IsManager).First();
                            obj.DOB = Employeelist.Select(x => x.DOB).First();
                            obj.Marital_Status = Employeelist.Select(x => x.Marital_Status).First();
                            obj.Gender = Employeelist.Select(x => x.Gender).First();
                            obj.Blood_Group = Employeelist.Select(x => x.Blood_Group).First();
                            obj.Mobile_No = Employeelist.Select(x => x.Mobile_No).First();
                            obj.Email = Employeelist.Select(x => x.Email).First();
                            obj.PassPort_No = Employeelist.Select(x => x.PassPort_No).First();
                            obj.CitizenShip_No = Employeelist.Select(x => x.CitizenShip_No).First();
                            obj.Issued_Date = Employeelist.Select(x => x.Issued_Date).First();
                            obj.Issued_District = Employeelist.Select(x => x.Issued_District).First();
                            obj.Religion = Employeelist.Select(x => x.Religion).First();
                            obj.photo = imgName;
                            obj.FullPath = imgFullPath;
                            obj.Permanet_Address = Employeelist.Select(x => x.Permanet_Address).First();
                            obj.Permanet_Nepali = Employeelist.Select(x => x.Permanet_Nepali).First();
                            obj.Temp_Address = Employeelist.Select(x => x.Temp_Address).First();
                            obj.Temp_Nepali = Employeelist.Select(x => x.Temp_Nepali).First();
                            obj.Marital_Status_Name = Employeelist.Select(x => x.Marital_Status_Name).First();
                            obj.GenderName = Employeelist.Select(x => x.GenderName).First();
                            obj.Blood_Group_Name = Employeelist.Select(x => x.Blood_Group_Name).First();
                            obj.Religion_Name = Employeelist.Select(x => x.Religion_Name).First();
                            obj.DepartmentName = Employeelist.Select(x => x.DepartmentName).First();
                            obj.DesignationName = Employeelist.Select(x => x.DesignationName).First();
                            obj.SectionName = Employeelist.Select(x => x.SectionName).First();

                        }
                        db.Close();
                        return obj;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeSalaryInfo GetEmployeeSalaryId(int id, string ComCode)
        {
            try
            {
                EmployeeSalaryInfo obj = new EmployeeSalaryInfo();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@Id", id);
                        var EmployeeSalary = db.Query<EmployeeSalaryInfo>(sql: "[dbo].[Usp_GetEmployeeSalary]", param: param, commandType: CommandType.StoredProcedure).ToList();


                        if (EmployeeSalary.Count() > 0)
                        {
                            obj.Id = Convert.ToInt32(EmployeeSalary.Select(x => x.Id).First());
                            obj.BasicSalary = EmployeeSalary.Select(x => x.BasicSalary).First();
                            obj.FromDate = EmployeeSalary.Select(x => x.FromDate).First();
                            obj.WorkOnRemote = EmployeeSalary.Select(x => x.WorkOnRemote).First();
                            obj.RemoteRebate = EmployeeSalary.Select(x => x.RemoteRebate).First();
                        }
                        db.Close();
                        return obj;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ShiftWO GetShiftByEmpId(int id, string ComCode)
        {
            try
            {
                ShiftWO obj = new ShiftWO();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@Emp_Id", id);
                        var Employeelist = db.Query<ShiftWO>(sql: "[dbo].[GetEmpShiftByEmpId]", param: param, commandType: CommandType.StoredProcedure);

                        if (Employeelist.Count() > 0)
                        {
                            obj.Id = Convert.ToInt32(Employeelist.Select(x => x.Id).First());
                            obj.ShiftId = Convert.ToInt32(Employeelist.Select(x => x.ShiftId).First());
                            obj.Emp_Id = Convert.ToInt32(Employeelist.Select(x => x.Emp_Id).First());
                            obj.WeeklyOff = Employeelist.Select(x => x.WeeklyOff).First();
                            string[] off = new string[] { Convert.ToString(obj.WeeklyOff) };
                            string[] offday = off[0].Split(',').ToArray();
                            int i = 0;
                            foreach (var c in offday)
                            {
                                if (c == "1" && i == 0)
                                {
                                    obj.Sunday = true;
                                }
                                if (c == "2" && i == 1)
                                {
                                    obj.Monday = true;
                                }
                                if (c == "3" && i == 2)
                                {
                                    obj.Tuesday = true;
                                }
                                if (c == "4" && i == 3)
                                {
                                    obj.Wednesday = true;

                                }
                                if (c == "5" && i == 4)
                                {
                                    obj.Thursday = true;

                                }
                                if (c == "6" && i == 5)
                                {
                                    obj.Friday = true;

                                }
                                if (c == "7" && i == 6)
                                {
                                    obj.Saturday = true;

                                }

                                i++;
                            }
                        }
                        db.Close();
                        return obj;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Allowance> GetAllowances(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@Id", id);
                        var Allowancelist = db.Query<Allowance>(sql: "[dbo].[Usp_GetAllowanceBySalaryId]", param: param, commandType: CommandType.StoredProcedure);
                        return Allowancelist.ToList();
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DeductionRebateTax> GetDeductionRebates(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@Id", id);
                        var DeductionRebateList = db.Query<DeductionRebateTax>(sql: "[dbo].[Usp_GetDeductionRebateBySalaryId]", param: param, commandType: CommandType.StoredProcedure);
                        return DeductionRebateList.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<EmployeeAttendaceInfo> EmployeeAttendaceInfo(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@EmployeeId", id);
                        var data = db.Query<EmployeeAttendaceInfo>(sql: "[dbo].[EmployeeAttendanceDetail]", param: param, commandType: CommandType.StoredProcedure);
                        return data;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<EmployeeLeaveInfo> EmployeeLeaveInfo(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@EmployeeId", id);
                        var data = db.Query<EmployeeLeaveInfo>(sql: "[dbo].[EmployeeLeaveDetail]", param: param, commandType: CommandType.StoredProcedure);
                        return data;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<EmployeePayrollInfo> EmployeePayrollInfo(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@EmployeeId", id);
                        var data = db.Query<EmployeePayrollInfo>(sql: "[dbo].[EmployeePayrollDetail]", param: param, commandType: CommandType.StoredProcedure);
                        return data;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<EmployeeLeaveDetail> EmployeeLeaveDetail(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@EmpId", id);
                        var data = db.Query<EmployeeLeaveDetail>(sql: "[dbo].[EmployeeAllLeaveDetail]", param: param, commandType: CommandType.StoredProcedure);
                        return data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //For Create Leave 
        public string CreateLeave(string ComCode, int EmpId, int lId, decimal lDay, decimal TLTaken, decimal LRem, string Des, string FDateNp, string TDateNp, DateTime FDate, DateTime TDate)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", "I");
                    param.Add("@Id", 0);
                    param.Add("@EmployeeId", EmpId);
                    param.Add("@LeaveId", lId);
                    param.Add("@LeaveDay", "1");
                    param.Add("@TotalLeaveTaken", TLTaken);
                    param.Add("@LeaveRemaining", LRem);
                    param.Add("@Description", Des);
                    param.Add("@ApprovedBy", 0);
                    param.Add("@IsDeleted", 0);
                    param.Add("@FromDateNepali", FDateNp);
                    param.Add("@ToDateNepali", TDateNp);
                    param.Add("@FromDate", FDate);
                    param.Add("@ToDate", TDate);
                    param.Add("@CreatedBy", 0);
                    param.Add("@CreatedDate", DateTime.Now);
                    param.Add("@Status", "P");
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_LeaveApplication]", param: param, commandType: CommandType.StoredProcedure);

                    db.Close();
                    int retId = param.Get<Int16>("@Return_Id");
                    if (retId > 0)
                    {
                        return "Leave Requested";
                    }
                    return "Leave Request Failed";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //For Check Leave on same date
        public string CheckLeave(string ComCode, int EmpId, int LeaveId, DateTime FromDate, DateTime ToDate)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<int>("select Count(*) Count from LeaveApplication where @Date between FromDate and ToDate and EmployeeId= @Id and IsDeleted=0 and id!=@lId and Status!= 'R'", param: new { Date = FromDate, Id = EmpId, @lId = LeaveId }, commandType: CommandType.Text).SingleOrDefault();
                    //var s = data.Count();

                    db.Close();
                    if (data == 0)
                    {
                        return "NoLeave";
                    }
                    else
                    {
                        return "Pending Leave Already Exist on this Date";
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<LeaveDetail> LeaveDetail(int id, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        db.Open();

                        var fy = db.Query<FiscalYear>(sql: @"Select * from FiscalYears where CurrentFiscalYear = 1", commandType: CommandType.Text).SingleOrDefault();

                        if(fy == null)
                        {
                            return null;
                        }
                        int fyId = fy.Id;
                        //DateTime StartDate = Convert.ToDateTime(fy.StartDate.ToString("yyyy-MM-dd"));
                        //DateTime EndDate = Convert.ToDateTime(fy.EndDate.ToString("yyyy-MM-dd"));

                        string StartDate = fy.StartDate.ToString("yyyy-MM-dd");
                        string EndDate = fy.EndDate.ToString("yyyy-MM-dd");

                        var data = db.Query<LeaveDetail>(sql: @"Select ep.Id EmpId, ep.Emp_Name EmployeeName, lm.LeaveName, lad.NoOfDays AssignedLeaveDays, lad.LeaveId
                                                                from LeaveAssign la 
                                                                join LeaveAssignDetail lad on la.Id = lad.LeaveAssignId
                                                                Left join Employee ep on la.EmployeeId = ep.Id
                                                                Left join LeaveMaster lm on lad.LeaveId = lm.Id
                                                                Left join LeaveApplication lva on ep.Id = lva.EmployeeId
                                                                Where la.EmployeeId = " + id + @" and la.FiscalYearId = "+ fyId+ @"
                                                                Group by lad.LeaveId, lad.NoOfDays, lm.LeaveName, ep.Emp_Name, ep.Id", commandType: CommandType.Text);
                        foreach (var item in data)
                        {
                            item.LeaveTakenDetail = db.Query<LeaveTakenDetail>(sql: @"Select lm.LeaveName, Convert(varchar, la.FromDate, 23) FromDate, Convert(varchar,la.ToDate,23) ToDate, la.FromDateNepali, la.ToDateNepali
                                                                                        ,(CASE WHEN la.LeaveDay = 1
                                                                                        	THEN
                                                                                        	(CASE WHEN (la.FromDate between '"+ StartDate + "' and '" + EndDate + "') and (la.ToDate between '" + StartDate + "' and '" + EndDate + @"')
                                                                                        		  THEN DATEDIFF(Day, la.FromDate, la.ToDate) + 1
                                                                                        		  WHEN (FromDate between '" + StartDate + "' and '" + EndDate + "') and (la.ToDate > '" + EndDate + @"')
                                                                                        		  THEN DATEDIFF(Day, la.FromDate, " + EndDate + @") + 1
                                                                                        		  WHEN (ToDate between '" + StartDate + "' and '" + EndDate + "') and (la.FromDate < '" + StartDate + @"')
                                                                                        		  THEN DATEDIFF(Day, '" + StartDate + @"', la.ToDate) + 1
                                                                                        		END)
                                                                                        	ELSE 0.5 END)
                                                                                        		 as TakenLeaveDays
                                                                                        , CASE WHEN la.Status = 'P' THEN 'Pending' WHEN la.Status = 'R' THEN 'Reject' WHEN la.Status = 'A' THEN 'Approved' END Status
                                                                                        from LeaveApplication la
                                                                                        left join LeaveMaster lm on la.LeaveId = lm.Id
                                                                                        Where la.EmployeeId = " + item.EmpId+" and lm.Id = "+item.LeaveId+ "", commandType: CommandType.Text);
                        }
                        return data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}