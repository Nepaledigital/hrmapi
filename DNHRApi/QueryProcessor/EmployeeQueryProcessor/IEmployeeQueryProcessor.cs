﻿using Entity.Model;
using System;
using System.Collections.Generic;

namespace QueryProcessor
{
	public	interface IEmployeeQueryProcessor
	{
		IEnumerable<Employee> GetAllData(string ComCode);
		Employee GetById(int id, string ComCode);
        EmployeeSalaryInfo GetEmployeeSalaryId(int id, string ComCode);
		ShiftWO GetShiftByEmpId(int id, string ComCode);
        List<Allowance> GetAllowances(int id, string ComCode);
        List<DeductionRebateTax> GetDeductionRebates(int id, string ComCode);
        IEnumerable<EmployeeAttendaceInfo> EmployeeAttendaceInfo(int id, string ComCode);
        IEnumerable<EmployeeLeaveInfo> EmployeeLeaveInfo(int id, string ComCode);
        IEnumerable<EmployeePayrollInfo> EmployeePayrollInfo(int id, string ComCode);
        IEnumerable<EmployeeLeaveDetail> EmployeeLeaveDetail(int id, string ComCode);
        string CreateLeave(string ComCode, int EmpId, int lId, decimal lDay, decimal TLTaken, decimal LRem, string Des, string FDateNp, string TDateNp, DateTime FDate, DateTime TDate);
        string CheckLeave(string ComCode, int EmpId, int LeaveId, DateTime FromDate, DateTime ToDate);
        IEnumerable<LeaveDetail> LeaveDetail(int id, string ComCode);
    }
}
