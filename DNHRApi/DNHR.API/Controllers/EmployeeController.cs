﻿using Entity;
using QueryProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DNHR.API.Controllers
{
    public class EmployeeController : BaseController
    {
        AllQueryProcessor QueryProcessor = new AllQueryProcessor();
        public JsonResult GetEmployeeList(string ComCode, string rolename, int EmpId)
        {
            var data = QueryProcessor.EmployeeQueryProcessor.GetAllData(ComCode);
            var data1 = QueryProcessor.EmployeeQueryProcessor.GetAllData(ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.Id == Convert.ToInt32(EmpId)).ToList();
            }
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeBasicInfo(int id, string ComCode)
        {
            var BasicDetal = QueryProcessor.EmployeeQueryProcessor.GetById(id, ComCode);
            var result = new { success = true, body = BasicDetal, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeSalaryDetailInfo(int id, string ComCode)
        {
            var data = QueryProcessor.EmployeeQueryProcessor.GetById(id, ComCode);
            data.EmployeeSalaryInfo = QueryProcessor.EmployeeQueryProcessor.GetEmployeeSalaryId(id, ComCode);
            if (data.EmployeeSalaryInfo != null && data.EmployeeSalaryInfo.Id != 0)
            {
                data.Allowances = QueryProcessor.EmployeeQueryProcessor.GetAllowances(data.EmployeeSalaryInfo.Id, ComCode);
                foreach (var item in data.Allowances)
                {
                    item.AllowancesList = CGlobalModels.BindAllowances(string.Empty, string.Empty, string.Empty, string.Empty, ComCode);
                }
                data.DeductionRebates = QueryProcessor.EmployeeQueryProcessor.GetDeductionRebates(data.EmployeeSalaryInfo.Id, ComCode);
                foreach (var item in data.DeductionRebates)
                {
                    item.DeductionRebateList = CGlobalModels.BindDeductionRebate(string.Empty, string.Empty, string.Empty, string.Empty, ComCode);
                }

            }
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult EmployeeTimePunchInfo(int id)
        //{
        //    var data = empTimePunchQueryProcessor.GetByEmpId(id);
        //    data.Emp_Id = id;
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult EmployeeShiftInfo(int id, string ComCode)
        {
            ViewBag.Shift = CGlobalModels.BindShift(string.Empty, string.Empty, string.Empty, string.Empty, ComCode);
            var data = QueryProcessor.EmployeeQueryProcessor.GetShiftByEmpId(id, ComCode);
            data.Emp_Id = id;
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeAttendanceInfo(int id, string ComCode)
        {
            var data = QueryProcessor.EmployeeQueryProcessor.EmployeeAttendaceInfo(id, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeLeaveInfo(int id, string ComCode)
        {
            var data = QueryProcessor.EmployeeQueryProcessor.EmployeeLeaveInfo(id, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeePayrollInfo(int id, string ComCode)
        {
            var data = QueryProcessor.EmployeeQueryProcessor.EmployeePayrollInfo(id, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EmployeeLeaveDetail(int id, string ComCode)
        {
            var data = QueryProcessor.EmployeeQueryProcessor.EmployeeLeaveDetail(id, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LeaveRequest(string ComCode, string EmpId,string LeaveId, string FromDate, string ToDate, string LeaveDay, string TotalLeaveTaken, string RemainingDays, string Description)
        {
            DateTime fromDate = Convert.ToDateTime(FromDate);
            DateTime toDate = Convert.ToDateTime(ToDate);

            var data = QueryProcessor.EmployeeQueryProcessor.CheckLeave(ComCode, Convert.ToInt32(EmpId), Convert.ToInt32(LeaveId), fromDate, toDate);
            if (data == "NoLeave")
            {
                var ToDateNep = NepDateConverter.EngToNep(Convert.ToInt32(toDate.Year), Convert.ToInt32(toDate.Month), Convert.ToInt32(toDate.Day)).ToString();
                var FromDateNep = NepDateConverter.EngToNep(Convert.ToInt32(fromDate.Year), Convert.ToInt32(fromDate.Month), Convert.ToInt32(fromDate.Day)).ToString();

                var res = QueryProcessor.EmployeeQueryProcessor.CreateLeave(ComCode, Convert.ToInt32(EmpId), Convert.ToInt32(LeaveId), Convert.ToDecimal(LeaveDay), Convert.ToDecimal(TotalLeaveTaken), Convert.ToDecimal(RemainingDays), Description, FromDateNep, ToDateNep, fromDate, toDate);
                var result = new { success = true, body = "", message = res };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = true, body = "", message = data };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult LeaveDetail(int id, string ComCode)
        {
            var data = QueryProcessor.EmployeeQueryProcessor.LeaveDetail(id, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Test(int id, string ComCode)
        {
            //var data = QueryProcessor.EmployeeQueryProcessor.LeaveDetail(id, ComCode);
            var result = new { success = true, body = "Test`", message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}