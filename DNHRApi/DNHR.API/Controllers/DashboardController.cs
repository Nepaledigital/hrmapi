﻿using Entity;
using QueryProcessor;
using QueryProcessor.DashboardQueryProcessor;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DNHR.API.Controllers
{
    public class DashboardController : BaseController
    {
        private AllQueryProcessor QueryProcessor = new AllQueryProcessor();
        public JsonResult AttendanceReport(string ComCode)
        {

            var Compcode = Request.ServerVariables.Get("HTTP_USER_AGENT");
            ComCode = Request.QueryString["ComCode"];
            if (ComCode != null)
            {
                var data = QueryProcessor.DashboardProcessor.GetAllData(ComCode);
              
                var result = new { success = true, body = data, message = "" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, body = "", message = "Please enter correct company code !!" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeeOnLeave(string ComCode, int EmpId)
        {
            var data = QueryProcessor.DashboardProcessor.GetEmployeeOnLeave(ComCode);
            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(EmpId, ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.Id == EmpId);
            }
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeaveReport(string ComCode, int EmpId)
        {
            var data = QueryProcessor.DashboardProcessor.GetLeaveReport(ComCode);
           // string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(EmpId, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeaveReportWeekly(string ComCode, int EmpId)
        {
            var data = QueryProcessor.DashboardProcessor.GetLeaveReport(ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeaveReportMonthly(string ComCode, int EmpId)
        {
            var data = QueryProcessor.DashboardProcessor.GetLeaveReport(ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MonthlyReport(string ComCode, string year, string month, string DateType, string empId, string deptId)
        {
            ViewBag.DateType = DateType;
            int Year = Convert.ToInt32(year);
            int Month = Convert.ToInt32(month);
            var TotalDays = DateTime.DaysInMonth(Year, Month);


            string[] Days = new string[TotalDays];
            if (DateType == "Nepali")
            {
                TotalDays = NepDateConverter.DaysByYearMonth(Year, Month);
                ViewBag.TotalDays = TotalDays;
                Days = new string[TotalDays];
                for (int i = 0; i < TotalDays; i++)
                {
                    DateTime EngDate = NepDateConverter.NepToEng(Year, Month, i + 1);
                    Days[i] = EngDate.ToString("ddd");
                }

                ViewBag.Days = Days;

            }
            else
            {
                ViewBag.TotalDays = TotalDays;
                for (int i = 0; i < TotalDays; i++)
                {
                    DateTime dt = new DateTime(Year, Month, i + 1);
                    Days[i] = dt.ToString("ddd");
                }
                ViewBag.Days = Days;
            }
            var color = QueryProcessor.AttendanceQueryProcessor.GetColor(ComCode);
            if (color == null)
            {
                Color Color = new Color();
                Color.LateInColor = "#e06666";
                Color.EarlyOutColor = "ea9999";
                Color.WeekendColor = "#98d0dd";
                Color.AbsentColor = "#8c0404";
                Color.HolidayColor = "#eeeeee";
                Color.ShiftNotAsignColor = "#ff0000";
                Color.AbsentByLE = "#f50057";
                Color.DidNotLogoutColor = "#eeeeee";
                Color.LeaveColor = "#cc0000";
                Color.PresentColor = "rgb(84, 142, 48)";
                ViewBag.Color = Color;
            }
            else
            {
                ViewBag.Color = color;
            }
            ViewBag.Year = Year;
            ViewBag.Month = Month;

            int EmployeeId = empId == "" ? 0 : Convert.ToInt32(empId);
            var data = QueryProcessor.AttendanceQueryProcessor.GetMonthlyReport(ComCode, Year, Month, DateType, EmployeeId, deptId).ToList();

            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(Convert.ToInt32(empId), ComCode);
            // if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(empId)).ToList();
            }
            ViewBag.HolidayList = QueryProcessor.AttendanceQueryProcessor.GetHolidays(Year, Month, ComCode);
            ViewBag.LeaveList = QueryProcessor.AttendanceQueryProcessor.LeaveDetails(ComCode);
            ViewBag.LeaveCodeList = QueryProcessor.AttendanceQueryProcessor.LeaveCodeList(ComCode);
            DataTable dtl = General.ToDataTable(data);
            return PartialView("MonthlyReport", dtl);
        }

        public ActionResult DailyReport(string ComCode, string Date, string DateType, string EmpId, string DeptId, string DesignationId, string DeviceCode)
        {
            var color = QueryProcessor.AttendanceQueryProcessor.GetColor(ComCode);
            ViewBag.Color = color;
            var data = QueryProcessor.AttendanceQueryProcessor.GetDailyReport(ComCode, Date, DateType, EmpId, DeptId, DesignationId, DeviceCode);
            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(Convert.ToInt32(EmpId), ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.Id == Convert.ToInt32(EmpId)).ToList();
            }
            return PartialView("DailyReport", data);
        }

        public ActionResult YearlyReport(string ComCode, string Year, string DateType, string EmpId, string desId, string deptId)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetAllYearlyData(ComCode, Year, DateType, EmpId, desId, deptId);
            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(Convert.ToInt32(EmpId), ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.Id == Convert.ToInt32(EmpId)).ToList();
            }

            var color = QueryProcessor.AttendanceQueryProcessor.GetColor(ComCode);
            ViewBag.Color = color;
            ViewBag.DateType = DateType;
            if (DateType == "Nepali")
                ViewBag.CurrentYear = NepDateConverter.EngToNep(DateTime.Now).Year;
            else
                ViewBag.CurrentYear = DateTime.Now.Year;
            return PartialView("YearlyReport", data);
        }

        public ActionResult SummaryReport(string ComCode, string fromDate, string toDate, string DateType, string empId, string deptId)
        {
            int CompanyCode = Convert.ToInt32(ComCode);
            //ViewBag.ComapnyInfo = QP.CompanyNameById(CompanyCode);

           
            if (DateType == "Nepali")
            {
                ViewBag.FromDate = fromDate;
                ViewBag.ToDate = toDate;

                var EngToDateByNepali = NepDateConverter.NepToEng(Convert.ToInt32(toDate.Split('-')[0]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[2]));
                toDate = EngToDateByNepali.ToString();

                var EngFromDateByNepali = NepDateConverter.NepToEng(Convert.ToInt32(fromDate.Split('-')[0]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[2]));
                fromDate = EngFromDateByNepali.ToString();
            }
            else
            {
                DateTime FromDate = Convert.ToDateTime(fromDate);
                DateTime ToDate = Convert.ToDateTime(toDate);

                ViewBag.FromDate = FromDate.Date.ToShortDateString();
                ViewBag.ToDate = ToDate.Date.ToShortDateString();
            }

            var data = QueryProcessor.AttendanceQueryProcessor.GetEmpSummaryDetails(ComCode, empId, deptId, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(Convert.ToInt32(empId), ComCode);
            // if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(empId)).ToList();
            }

            return PartialView("SummaryReport", data);
        }

        public ActionResult DetailReport(string ComCode, string fromDate, string toDate, string DateType, string empId, string deptId)
        {
            if (DateType == "Nepali")
            {
                ViewBag.FromDate = fromDate;
                ViewBag.ToDate = toDate;

                var EngToDateByNepali = NepDateConverter.NepToEng(Convert.ToInt32(toDate.Split('-')[0]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[2]));
                toDate = EngToDateByNepali.ToString();

                var EngFromDateByNepali = NepDateConverter.NepToEng(Convert.ToInt32(fromDate.Split('-')[0]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[2]));
                fromDate = EngFromDateByNepali.ToString();
            }
            else
            {
                DateTime FromDate = Convert.ToDateTime(fromDate);
                DateTime ToDate = Convert.ToDateTime(toDate);

                ViewBag.FromDate = FromDate.Date.ToShortDateString();
                ViewBag.ToDate = ToDate.Date.ToShortDateString();
            }

            //ViewBag.EmployeeSearched = empId == "" ? "" : empId == "0" ? "" : "Yes";
            ViewBag.LList = QueryProcessor.AttendanceQueryProcessor.GetLeaveDetails(ComCode, empId, deptId, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
            ViewBag.HList = QueryProcessor.AttendanceQueryProcessor.GetHolidayDetails(ComCode, empId, deptId, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));

            var usrData = QueryProcessor.AttendanceQueryProcessor.GetAttendanceDetails(ComCode, empId, deptId, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
          

            return PartialView("DetailReport", usrData);
        }
    }
}